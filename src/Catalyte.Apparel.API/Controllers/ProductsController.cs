﻿﻿using AutoMapper;
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Drawing;
using Catalyte.Apparel.DTOs.Products;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Data.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using Catalyte.Apparel.Data.Model;
using System.Reflection;

namespace Catalyte.Apparel.API.Controllers
{
    /// <summary>
    /// The ProductsController exposes endpoints for product related actions.
    /// </summary>
    [ApiController]
    [Route("/products")]
    public class ProductsController : ControllerBase
    {
        private readonly ILogger<ProductsController> _logger;
        private readonly IProductProvider _productProvider;
        private readonly IMapper _mapper;

        public ProductsController(
            ILogger<ProductsController> logger,
            IProductProvider productProvider,
            IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _productProvider = productProvider;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetProductsAsync()
        {
            _logger.LogInformation("Request received for GetProductsAsync");

            var products = await _productProvider.GetProductsAsync();
            var productDTOs = _mapper.Map<IEnumerable<ProductDTO>>(products);

            return Ok(productDTOs);
        }

        [HttpGet("/products/{id:int}")]
        public async Task<ActionResult<ProductDTO>> GetProductByIdAsync(int id)
        {
            _logger.LogInformation($"Request received for GetProductByIdAsync for id: {id}");

            var product = await _productProvider.GetProductByIdAsync(id);
            var productDTO = _mapper.Map<ProductDTO>(product);

            return Ok(productDTO);
        }

        [HttpGet("/products/categories")]
        public async Task<ActionResult> GetProductCategoriesAsync()
        {
            var products = await _productProvider.GetProductsAsync();
            var productCategories = from product in products select product.Category;

            return Ok(productCategories.Distinct());

        }

        [HttpGet("/products/types")]
        public async Task<ActionResult> GetProductTypesAsync()
        {
            var products = await _productProvider.GetProductsAsync();
            var productTypes = from product in products select product.Type;

            return Ok(productTypes.Distinct());

        }
        [HttpGet("/products/status/active")]
        public async Task<ActionResult> GetActiveProducts()
        {
            var products = await _productProvider.GetProductsAsync();
            var activeProducts = from product in products where product.Active is true select product;

            return Ok(activeProducts);
        }
        [HttpGet("/products/filter")]
        public async Task<ActionResult> GetFilteredProducts([FromQuery] ProductFilterDTO filter)
        {
            var products = await _productProvider.GetFilteredProductsAsync(filter);
            
            return Ok(products);
        }
        [HttpPost("/products")]
        public async Task<ActionResult<ProductDTO>> CreateProductAsync([FromBody] Product productToCreate)
        {
            _logger.LogInformation("Request received for CreateProductsAsync");

            var newProduct = await _productProvider.CreateProductAsync(productToCreate);
            var productDTO = _mapper.Map<ProductDTO>(newProduct);

            return Created("/products", productDTO);
        }
        [HttpGet("/products/brands")]
        public async Task<ActionResult> GetProductBrandsAsync()
        {
            var products = await _productProvider.GetProductsAsync();
            var productBrands = from product in products select product.Brand;

            return Ok(productBrands.Distinct());

        }

        [HttpGet("/products/demographics")]
        public async Task<ActionResult> GetProductDemographicsAsync()
        {
            var products = await _productProvider.GetProductsAsync();
            var productDemographics = from product in products select product.Demographic;

            return Ok(productDemographics.Distinct());

        }

        [HttpGet("/products/primarycolor")]
        public async Task<ActionResult> GetProductPrimaryColorsAsync()
        {
            var products = await _productProvider.GetProductsAsync();
            var productPrimaryColor = from product in products select product.PrimaryColorCode;

            return Ok(productPrimaryColor.Distinct());

        }

        [HttpGet("/products/material")]
        public async Task<ActionResult> GetProductMaterialsAsync()
        {
            var products = await _productProvider.GetProductsAsync();
            var productMaterials = from product in products select product.Material;

            return Ok(productMaterials.Distinct());

        }

    }
}
