﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Catalyte.Apparel.API.DTOMappings;
using Catalyte.Apparel.Data.Filters.PaginationFilters;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.DTOs.Products;
using Catalyte.Apparel.Providers.Helpers;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Catalyte.Apparel.API.Controllers
{
    [ApiController]
    [Route("/WishList")]
    public class WishListController : ControllerBase
    {
        private readonly ILogger<WishListController> _logger;
        private readonly IWishListProvider _wishListProvider;
        private readonly IMapper _mapper;

        public WishListController(
            ILogger<WishListController> logger,
            IWishListProvider wishListProvider,
            IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _wishListProvider = wishListProvider;
        }
        [HttpPost]
        public async Task<ActionResult<WishListDTO>> CreateWishListItemAsync([FromBody] WishList wishListItemToCreate)
        {
            _logger.LogInformation("Request received for CreateWishListItemAsync");

            var WishListItem = await _wishListProvider.CreateWishListItemAsync(wishListItemToCreate);
            var wishListDTO = _mapper.Map<WishListDTO>(WishListItem);

            return Created("/wishList", wishListDTO);
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<WishListDTO>>> GetReviewsAsync()
        {
            var wishList = await _wishListProvider.GetWishListAsync();
            var wishListDTOs = _mapper.Map<IEnumerable<WishListDTO>>(wishList);

            return Ok(wishListDTOs);

        }
        [HttpGet("{email}")]
        public async Task<ActionResult<List<WishListDTO>>> GetWishListItemsByEmailAsync(string email)
        {
            var emailCheck = new EmailAddressAttribute();
            if (email == null)
            {
                throw new BadRequestException($"An email address must be included:");
            }
            else
            {
                email = email.Trim();
                if (!emailCheck.IsValid(email))
                {
                    throw new BadRequestException($"{email} is not a valid URL");
                }
            }
            var wishList = await _wishListProvider.GetWishListByEmailAsync(email);

            var wishListDTO = _mapper.MapWishListsToWishListDTOs(wishList);

            return Ok(wishListDTO);
        }

        [HttpGet("filter/{email}")]
        public async Task<ActionResult> GetPaginatedWishListByEmail(string email, [FromQuery] PaginationFilter paginationFilter)
        {
            var validFilter = new PaginationFilter(paginationFilter.PageNumber, paginationFilter.PageSize);
            var wishList = await _wishListProvider.GetWishListByEmailAsync(email);
            var totalRecords = wishList.Count;
            var pagedWishList = wishList.Skip((paginationFilter.PageNumber - 1) * paginationFilter.PageSize)
                    .Take(paginationFilter.PageSize);
            var pagedResponse = PaginationHelper.CreatePagedReponse<WishList>(pagedWishList.ToList(), validFilter, totalRecords);

            return Ok(pagedResponse.Data);
        }
        [HttpGet("maxNumber/{email}")]
        public async Task<ActionResult> GetPaginatedWishListByEmailMaxNumber(string email)
        {
            var wishList = await _wishListProvider.GetWishListByEmailAsync(email);
            var totalRecords = wishList.Count;

            return Ok(totalRecords);
        }
    }
}
