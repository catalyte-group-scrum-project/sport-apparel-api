﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.DTOs.Reviews;
using Catalyte.Apparel.Providers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Catalyte.Apparel.API.Controllers
{
    /// <summary>
    /// The ReviewsController exposes endpoints for review related actions.
    /// </summary>
    [ApiController]
    [Route("/reviews")]
    public class ReviewsController : ControllerBase
    {
        private readonly ILogger<ReviewsController> _logger;
        private readonly IReviewProvider _reviewProvider;
        private readonly IMapper _mapper;

        public ReviewsController(
            ILogger<ReviewsController> logger,
            IReviewProvider reviewProvider,
            IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _reviewProvider = reviewProvider;
        }
        
        [HttpPost]
        public async Task<ActionResult<ReviewDTO>> CreateReviewAsync([FromBody] Review reviewToCreate)
        {
            _logger.LogInformation("Request received for CreateReviewAsync");

            var review = await _reviewProvider.CreateReviewAsync(reviewToCreate);
            var reviewDTO = _mapper.Map<ReviewDTO>(review);

            return Created("/review", reviewDTO);
        }
        
        [HttpGet("/reviews")]
        public async Task<ActionResult<IEnumerable<ReviewDTO>>> GetReviewsAsync()
        {
            var reviews = await _reviewProvider.GetReviewsAsync();
            var reviewDTOs = _mapper.Map<IEnumerable<ReviewDTO>>(reviews);

            return Ok(reviewDTOs);

        }

        [HttpGet("/reviews/{id:int}")]
        public async Task<ActionResult<ReviewDTO>> GetReviewByReviewIdAsync(int id)
        {
            _logger.LogInformation($"Request received for GetReviewByReviewIdAsync for id: {id}");

            var review = await _reviewProvider.GetReviewByReviewIdAsync(id);
            var reviewDTO = _mapper.Map<ReviewDTO>(review);

            return Ok(reviewDTO);
        }

        [HttpGet("/reviews/productId/{productId:int}")]
        public async Task<ActionResult<IEnumerable<ReviewDTO>>> GetReviewByProductIdAsync(int productId)
        {
            _logger.LogInformation($"Request received for GetReviewByProductIdAsync for id: {productId}");

            var reviews = await _reviewProvider.GetReviewsByProductIdAsync(productId);
            var reviewDTOs = _mapper.Map<IEnumerable<ReviewDTO>>(reviews);

            return Ok(reviewDTOs);
        }

        [HttpDelete("/reviews/delete/{id:int}")]
        public async Task<ActionResult> DeleteReviewByReviewIdAsync(int id)
        {
            _logger.LogInformation($"Request received for DeleteReviewByReviewIdAsync for id: {id}");

            _ = await _reviewProvider.DeleteReviewByReviewIdAsync(id);

            return NoContent();
        }


        [HttpGet("{email}")]
        public async Task<ActionResult<IEnumerable<ReviewDTO>>> GetReviewsAsyncByEmail(string email)
        {
            var reviews = await _reviewProvider.GetReviewsByEmailAsync(email);
            var reviewDTOs = _mapper.Map<List<ReviewDTO>>(reviews);

            return Ok(reviewDTOs);

        }

    }   
}
