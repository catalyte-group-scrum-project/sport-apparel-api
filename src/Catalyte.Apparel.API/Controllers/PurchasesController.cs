﻿using AutoMapper;
using Catalyte.Apparel.API.DTOMappings;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Catalyte.Apparel.DTOs.Purchases;
using Catalyte.Apparel.Providers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;

namespace Catalyte.Apparel.API.Controllers
{
    /// <summary>
    /// The PurchasessController exposes endpoints for purchase related actions.
    /// </summary>
    [ApiController]
    [Route("/purchases")]
    public class PurchasesController : ControllerBase
    {
        private readonly ILogger<PurchasesController> _logger;
        private readonly IPurchaseProvider _purchaseProvider;
        private readonly IMapper _mapper;

        public PurchasesController(
            ILogger<PurchasesController> logger,
            IPurchaseProvider purchaseProvider,
            IMapper mapper
        )
        {
            _logger = logger;
            _purchaseProvider = purchaseProvider;
            _mapper = mapper;
        }

        [HttpGet]
        [HttpGet("{email}")]
        public async Task<ActionResult<List<PurchaseDTO>>> GetPurchasesByEmailAsync(string email)
        {
            var emailCheck = new EmailAddressAttribute();
            if (email == null)
            {
                throw new BadRequestException($"An email address must be included:");
            }
            else
            {
                email = email.Trim();
                if (!emailCheck.IsValid(email))
                {
                    throw new BadRequestException($"{email} is not a valid URL");
                }
            }
            bool[] user = new bool[1];
            var purchases = await _purchaseProvider.GetPurchasesByEmailAsync(email, user);
            if (!user[0])
            {
                throw new NotFoundException($"Could not find user with email: {email}");
            }
            var purchaseDTO = _mapper.MapPurchasesToPurchaseDtos(purchases);

            return Ok(purchaseDTO);
        }

        [HttpPost]
        public async Task<ActionResult<List<PurchaseDTO>>> CreatePurchaseAsync([FromBody] CreatePurchaseDTO model)
        {
            _logger.LogInformation("Request received for CreatePurchase");

            var newPurchase = _mapper.MapCreatePurchaseDtoToPurchase(model);
            var savedPurchase = await _purchaseProvider.CreatePurchasesAsync(newPurchase);
            var purchaseDTO = _mapper.MapPurchaseToPurchaseDto(savedPurchase);

            if (purchaseDTO == null)
            {
                return NoContent();
            }

            return Created($"/purchases/", purchaseDTO);
        }
    }
}
