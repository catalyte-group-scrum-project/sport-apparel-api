﻿using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using Catalyte.Apparel.DTOs.Purchases;
using Catalyte.Apparel.Providers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Catalyte.Apparel.Data.Model;

namespace Catalyte.Apparel.API.Controllers
{
    /// <summary>
    /// The PromoCodesController exposes endpoints for promo code related actions.
    /// </summary>
    [ApiController]
    [Route("/lineItem")]
    public class LineItemController : ControllerBase
    {
        private readonly ILogger<LineItemController> _logger;
        private readonly ILineItemProvider _lineItemProvider;
        private readonly IMapper _mapper;

        public LineItemController(
            ILogger<LineItemController> logger,
            ILineItemProvider lineItemProvider,
            IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _lineItemProvider = lineItemProvider;
        }

        [HttpGet("/lineItem")]
        public async Task<ActionResult<IEnumerable<LineItemDTO>>> GetLineItemsAsync()
        {
            _logger.LogInformation("Request received for GetLineItemsAsync");

            var lineItems = await _lineItemProvider.GetLineItemsAsync();
            var lineItemsDTO = _mapper.Map<IEnumerable<LineItemDTO>>(lineItems);

            return Ok(lineItemsDTO);
        }
    }
}
