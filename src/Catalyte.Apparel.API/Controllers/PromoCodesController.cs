﻿﻿using AutoMapper;
using System.Threading.Tasks;
using Catalyte.Apparel.DTOs.PromoCodes;
using Catalyte.Apparel.Providers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Catalyte.Apparel.Data.Model;

namespace Catalyte.Apparel.API.Controllers
{
    /// <summary>
    /// The PromoCodesController exposes endpoints for promo code related actions.
    /// </summary>
    [ApiController]
    [Route("/promocodes")]
    public class PromoCodesController : ControllerBase
    {
        private readonly ILogger<PromoCodesController> _logger;
        private readonly IPromoCodeProvider _promoCodeProvider;
        private readonly IMapper _mapper;

        public PromoCodesController(
            ILogger<PromoCodesController> logger,
            IPromoCodeProvider promoCodeProvider,
            IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _promoCodeProvider = promoCodeProvider;
        }

        [HttpPost]
        public async Task<ActionResult<PromoCodeDTO>> CreatePromoCodeAsync([FromBody] PromoCode promoCodeToCreate)
        {
            _logger.LogInformation("Request received for CreatePromoCodeAsync");

            var promoCode = await _promoCodeProvider.CreatePromoCodeAsync(promoCodeToCreate);
            var promoCodeDTO = _mapper.Map<PromoCodeDTO>(promoCode);

            return Created("/promocodes", promoCodeDTO);
        }
    }
}
