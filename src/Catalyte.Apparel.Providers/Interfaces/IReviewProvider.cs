﻿using Catalyte.Apparel.Data.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Providers.Interfaces
{
    /// <summary>
    /// This interface provides an abstraction layer for review related service methods.
    /// </summary>
    public interface IReviewProvider
    {
        Task<Review> CreateReviewAsync(Review newReview);

        Task<IEnumerable<Review>> GetReviewsAsync();

        Task<IEnumerable<Review>> GetReviewsByProductIdAsync(int productId);

        Task<IEnumerable<Review>> GetReviewsByEmailAsync(string email);

        Task<Review> GetReviewByReviewIdAsync(int id);

        Task<Review> DeleteReviewByReviewIdAsync(int id);

    }
}
