﻿using Catalyte.Apparel.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Providers.Interfaces
{
    public interface IWishListProvider
    {
        Task<WishList> CreateWishListItemAsync(WishList wishListItemToCreate);
        Task<IEnumerable<WishList>> GetWishListAsync();
        Task<List<WishList>> GetWishListByEmailAsync(string email);
    }
}
