﻿using System;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catalyte.Apparel.Data.Model;
using Microsoft.Extensions.Logging;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;

namespace Catalyte.Apparel.Providers.Providers
{
    /// <summary>
    /// This class provides the implementation of the IReviewProvider interface, providing service methods for reviews.
    /// </summary>
    public class ReviewProvider : IReviewProvider
    {
        private readonly ILogger<ReviewProvider> _logger;
        private readonly IReviewRepository _reviewRepository;

        public ReviewProvider(IReviewRepository reviewRepository, ILogger<ReviewProvider> logger)
        {
            _logger = logger;
            _reviewRepository = reviewRepository;
        }

        /// <summary>
        /// Persists a review to the database
        /// </summary>
        /// <param name="newReview"></param>
        /// <returns></returns>

        /// <exception cref="ServiceUnavailableException"></exception>

        public async Task<Review> CreateReviewAsync(Review newReview)
        {
            newReview.DateCreated = DateTime.UtcNow;
            newReview.DateModified = DateTime.UtcNow;

            Review Review;

            try
            {
                Review = await _reviewRepository.CreateReviewAsync(newReview);
                _logger.LogInformation("Review saved.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            return Review;

        }

        /// <summary>
        /// Asynchronously retrieves all reviews from the database.
        /// </summary>
        /// <returns>All reviews in the database.</returns>
        public async Task<IEnumerable<Review>> GetReviewsAsync()
        {
            IEnumerable<Review> reviews;

            try
            {
                reviews = await _reviewRepository.GetReviewsAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }


            return reviews;
        }

        /// <summary>
        /// Asynchronously retrieves the review with the provided id from the database.
        /// </summary>
        /// <param name="id">The id of the review to retrieve.</param>
        /// <returns>The product.</returns>
        public async Task<Review> GetReviewByReviewIdAsync(int id)
        {
            Review review;

            try
            {
                review = await _reviewRepository.GetReviewByReviewIdAsync(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            if (review == null || review == default)
            {
                _logger.LogInformation($"Review with id: {id} could not be found.");
                throw new NotFoundException($"Review with id: {id} could not be found.");
            }

            return review;
        }

        /// <summary>
        /// Asynchronously retrieves the review with the provided id from the database.
        /// </summary>
        /// <param name="productId">The id of the product to retrieve reviews for.</param>
        /// <returns>array of reviews.</returns>
        public async Task<IEnumerable<Review>> GetReviewsByProductIdAsync(int productId)
        {
            IEnumerable<Review> reviews;

            try
            {
                reviews = await _reviewRepository.GetReviewsByProductIdAsync(productId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            if (reviews == null || reviews == default)
            {
                _logger.LogInformation($"Reviews for product: {productId} could not be found.");
                throw new NotFoundException($"Reviews for product: {productId} could not be found.");
            }

            return reviews;
        }

        public async Task<Review> DeleteReviewByReviewIdAsync(int id)
        {
            Review reviewToDelete;
            try
            {
                reviewToDelete = await _reviewRepository.GetReviewByReviewIdAsync(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }
            try
            {
                reviewToDelete = await _reviewRepository.DeleteReviewAsync(reviewToDelete);
                _logger.LogInformation("Review Deleted");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            return reviewToDelete;
        }
        /// <summary>
        /// Asynchronously retrieves reviews from the database that contain the email.
        /// </summary>
        /// <returns>match reviews from the database.</returns>
        public async Task<IEnumerable<Review>> GetReviewsByEmailAsync(string email)
        {
            IEnumerable<Review> reviews;

            try
            {
                reviews = await _reviewRepository.GetReviewsByEmailAsync(email);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }


            return reviews;
        }

    }
}
