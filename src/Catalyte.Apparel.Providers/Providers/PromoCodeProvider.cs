﻿using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Catalyte.Apparel.Providers.Providers
{
	/// <summary>
	/// This class provides the implementation of the IPromoCodeProvider interface, providing service methods for promo codes.
	/// </summary>
	public class PromoCodeProvider : IPromoCodeProvider
    {
		private readonly ILogger<PromoCodeProvider> _logger;
		private readonly IPromoCodeRepository _promoCodeRepository;

		public PromoCodeProvider(IPromoCodeRepository promoCodeRepository, ILogger<PromoCodeProvider> logger)
		{
			_logger = logger;
			_promoCodeRepository = promoCodeRepository;
		}

		/// <summary>
		/// Persists a promo code to the database
		/// </summary>
		/// <param name="newPromoCode"></param>
		/// <returns></returns>
		/// <exception cref="BadRequestException"></exception>
		/// <exception cref="ServiceUnavailableException"></exception>
		public async Task<PromoCode> CreatePromoCodeAsync(PromoCode newPromoCode)
		{
			var exceptions = new List<String>();

			if (newPromoCode.Title == null || newPromoCode.Title == "")
			{
				exceptions.Add("Promo code must have a title field.");
			}

			if (newPromoCode.Description == null || newPromoCode.Description == "")
			{
				exceptions.Add("Promo code must have a description field.");
			}

            if (newPromoCode.IsPercent == null)
            {
                exceptions.Add("Promo code must have an IsPercent field.");
            }

            if (newPromoCode.Rate <= 0)
			{
				exceptions.Add("Promo code must have a rate greater than 0.");
			}

			if (exceptions.Count > 0)
            {
				_logger.LogError(string.Join(" ", exceptions));
				throw new BadRequestException(string.Join(" ", exceptions));
			}

			// SET TIMESTAMP
			newPromoCode.DateCreated = DateTime.UtcNow;
			newPromoCode.DateModified = DateTime.UtcNow;

			PromoCode savedPromoCode;

			try
			{
				savedPromoCode = await _promoCodeRepository.CreatePromoCodeAsync(newPromoCode);
				_logger.LogInformation("PromoCode saved.");
			}
			catch (Exception ex)
			{
				_logger.LogError(ex.Message);
				throw new ServiceUnavailableException("There was a problem connecting to the database.");
			}

			return savedPromoCode;
		}
	}
}
