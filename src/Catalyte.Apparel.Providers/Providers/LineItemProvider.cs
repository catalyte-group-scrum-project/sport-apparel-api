﻿using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Data.Repositories;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Providers.Providers
{
    public class LineItemProvider : ILineItemProvider
    {
        private readonly ILogger<LineItemProvider> _logger;
        private readonly ILineItemRepository _lineItemRepository;

        public LineItemProvider(ILineItemRepository lineItemRepostory, ILogger<LineItemProvider> logger)
        {
            _logger = logger;
            _lineItemRepository = lineItemRepostory;
        }
        public async Task<IEnumerable<LineItem>> GetLineItemsAsync()
        { 
            IEnumerable<LineItem> lineItems;

            try
            {
                lineItems = await _lineItemRepository.GetLineItemsAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }


            return lineItems;
        }
    }
}
