﻿using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Data.Repositories;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Providers.Providers
{
    public class WishListProvider : IWishListProvider
    {
        private readonly ILogger<WishListProvider> _logger;
        private readonly IWishListRepository _wishListRepository;

        public WishListProvider(IWishListRepository wishListRepository, ILogger<WishListProvider> logger)
        {
            _logger = logger;
            _wishListRepository = wishListRepository;
        }

        public async Task<WishList> CreateWishListItemAsync(WishList wishListItemToCreate)
        {
            WishList wishListItemCreated;
            wishListItemToCreate.DateModified = DateTime.Now;
            wishListItemToCreate.DateCreated = DateTime.Now;
            try
            {
                wishListItemCreated = await _wishListRepository.CreateWishListAsync(wishListItemToCreate);
                _logger.LogInformation("Wishlist item saved.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            return wishListItemCreated;
        }
        public async Task<IEnumerable<WishList>> GetWishListAsync()
        {
            IEnumerable<WishList> wishList;
            try
            {
                wishList = await _wishListRepository.GetWishListAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }
            return wishList;
        }

        public async Task<List<WishList>> GetWishListByEmailAsync(string email)
        {
            List<WishList> wishList;

            try
            {
                wishList = await _wishListRepository.GetWishListItemsByEmailAsync(email);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            if (wishList == default)
            {
                _logger.LogError($"Could not find wish lists with email: {email}");
                throw new NotFoundException($"Could not find wish lists with email: {email}");
            }

            return wishList;
        }
    }
}
