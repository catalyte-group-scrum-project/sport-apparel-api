﻿using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Data.Repositories;
using Catalyte.Apparel.Data.Context;
using Catalyte.Apparel.Data.Filters;
using Catalyte.Apparel.Providers.Providers;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace Catalyte.Apparel.Providers.Providers
{
    
    /// <summary>
    /// This class provides the implementation of the IPurchaseProvider interface, providing service methods for purchases.
    /// </summary>
    public class PurchaseProvider : IPurchaseProvider
    {
        private readonly ILogger<PurchaseProvider> _logger;
        private readonly IPurchaseRepository _purchaseRepository;
       
        public PurchaseProvider(IPurchaseRepository purchaseRepository, ILogger<PurchaseProvider> logger)
        {
            _logger = logger;
            _purchaseRepository = purchaseRepository;
           

        }

        /// <summary>
        /// Retrieves matching purchases from the database.
        /// </summary>
        /// <param name="page">Number of pages.</param>
        /// <param name="pageSize">How many purchases per page.</param>
        /// <returns>matching purchases.</returns>
        public async Task<IEnumerable<Purchase>> GetPurchasesByEmailAsync(string email, bool[] user)
        {
            List<Purchase> purchases;

            try
            {
                purchases = await _purchaseRepository.GetPurchasesByEmailAsync(email,user);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }
            return purchases;
        }

 

        /// <summary>
        /// Persists a purchase to the database as long as the product is of active status, returns error if criteria is not met.
        /// </summary>
        /// <param name="model">PurchaseDTO used to build the purchase.</param>
        /// <returns>The persisted purchase with IDs.</returns>
        /// 

        public async Task<Purchase> CreatePurchasesAsync(Purchase newPurchase)
        {
            Purchase savedPurchase;
 
            try
            {
                savedPurchase = await _purchaseRepository.CreatePurchaseAsync(newPurchase);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");

            }

            // CHECK TO MAKE SURE ALL PURCHASE ITEMS ARE ACTIVE

            List<int> inactiveProductIds = new();

            foreach (var product in savedPurchase.LineItems)
            {
                if(product.Product.Active == false)
                {
                    inactiveProductIds.Add(product.Product.Id);
                }
            }

            var listOfInactiveProducts = string.Join(", ", inactiveProductIds.Select(i => i.ToString()).ToArray());

            if(listOfInactiveProducts.Length > 0)
            {
                throw new UnprocessableEntityException("The product(s) with productId: " + listOfInactiveProducts + " is not available.");
            }

            var CreditCard = new CreditCard
            {
                CardNumber = savedPurchase.CardNumber,
                CVV = savedPurchase.CVV,
                CardHolder = savedPurchase.CardHolder,
                Expiration = savedPurchase.Expiration
            };

            try
            {
                Catalyte.Apparel.Data.Model.CreditCard.CreditCardValidation.Validation(CreditCard);
            }
            catch(Exception ex)
            {
                throw ex;
            }


           return savedPurchase;
        }
    }
}

