﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.DTOs.Products
{
    /// <summary>
    /// Describes a data transfer object for a review.
    /// </summary>
    public class WishListDTO
    {

        public int ProductId { get; set; }

        public string UserEmail { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ImageSrc { get; set; }

        public decimal Price { get; set; }

        public string Demographic { get; set; }

        public string Category { get; set; }

        public string Type { get; set; }
    }
}
