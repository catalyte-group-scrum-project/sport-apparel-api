﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.DTOs.Reviews
{
    /// <summary>
    /// Describes a data transfer object for a review.
    /// </summary>
    public class ReviewDTO
    {
        public int Id { get; set; }

        public int ProductId { get; set; }

        public string Title { get; set; }

        public string Comment { get; set; }

        public int Rating { get; set; }

        public string Email { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
