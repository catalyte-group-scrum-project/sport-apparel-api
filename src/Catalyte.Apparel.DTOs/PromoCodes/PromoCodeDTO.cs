﻿
namespace Catalyte.Apparel.DTOs.PromoCodes
{
    /// <summary>
    /// Describes a data transfer object for creating a promo code.
    /// </summary>
    public class PromoCodeDTO
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public bool? IsPercent { get; set; }
        public decimal Rate { get; set; }
    }
}
