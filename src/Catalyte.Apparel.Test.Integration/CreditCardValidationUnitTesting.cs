﻿using System;
using System.Linq;
using System.Collections.Generic;
using Xunit;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using Catalyte.Apparel.Data.Model;


namespace Validation.Test
{

    public class CreditCardValidationTest
    {

        CreditCard creditCard;

        public CreditCardValidationTest()
        {
            creditCard = new CreditCard();
        }

        [Fact]
        public void ValidationTest_empty_returnsException()
        {
            var creditCard = new CreditCard()
            {
                CardHolder = "",
                CardNumber = "",
                Expiration = "",
                CVV = ""

            };

            try
            {
                CreditCard.CreditCardValidation.Validation(creditCard);
            }
            catch (BadRequestException ex)
            {
                Assert.Contains("Credit card inputs cannot be empty", ex.Value.ErrorMessage);
            }

        }

        [Fact]
        public void ValidationTest_NotApprovedCard_returnsException()
        {
            var creditCard = new CreditCard()
            {
                CardHolder = "Max Perkins",
                CardNumber = "1435678998761234",
                Expiration = "11/21",
                CVV = "456"

            };
            
            try
            {
                CreditCard.CreditCardValidation.Validation(creditCard);
            }
            catch (BadRequestException ex)
            {
                Assert.Contains("2022 Sports Apparel, Inc. only accepts Visa, Mastercard, or AmEx", ex.Value.ErrorMessage);
            }
        }

        [Fact]
        public void ValidationTest_CardLengthToShort_returnsException()
        {
            var creditCard = new CreditCard()
            {
                CardHolder = "Max Perkins",
                CardNumber = "51356789987612",
                Expiration = "11/21",
                CVV = "456"

            };

            try
            {
                CreditCard.CreditCardValidation.Validation(creditCard);
            }
            catch (BadRequestException ex)
            {
                Assert.Contains("CardNumber must be 15 or 16 digits", ex.Value.ErrorMessage);
            }
        }

        [Fact]
        public void ValidationTest_CVVLessThan3_returnsException()
        {
            var creditCard = new CreditCard()
            {
                CardHolder = "Max Perkins",
                CardNumber = "5126489534786528",
                Expiration = "11/21",
                CVV = "4"

            };

            try
            {
                CreditCard.CreditCardValidation.Validation(creditCard);
            }
            catch (BadRequestException ex)
            {
                Assert.Contains("CVV must be 3 or 4 digits", ex.Value.ErrorMessage);
            }
        }

        [Fact]
        public void ValidationTest_CVVMoreThan3_returnsException()
        {
            var creditCard = new CreditCard()
            {
                CardHolder = "Max Perkins",
                CardNumber = "5126489534786528",
                Expiration = "11/23",
                CVV = "46575"

            };

            try
            {
                CreditCard.CreditCardValidation.Validation(creditCard);
            }
            catch (BadRequestException ex)
            {
                Assert.Contains("CVV must be 3 or 4 digits", ex.Value.ErrorMessage);
            }
        }

        [Fact]
        public void ValidationTest_ExpiredDate_returnsException()
        {
            var creditCard = new CreditCard()
            {
                CardHolder = "Max Perkins",
                CardNumber = "5126489534786528",
                Expiration = "01/22",
                CVV = "465"

            };

            try
            {
                CreditCard.CreditCardValidation.Validation(creditCard);
            }
            catch (BadRequestException ex)
            {
                Assert.Contains("Card must not be expired", ex.Value.ErrorMessage);
            }
        }

        [Fact]
        public void ValidationTest_SpacesInCardNumber_returnsNoExceptions()
        {
            var creditCard = new CreditCard()
            {
                CardHolder = "Max Perkins",
                CardNumber = "5126 4895 3478 6528",
                Expiration = "01/23",
                CVV = "465"

            };

            try
            {
                CreditCard.CreditCardValidation.Validation(creditCard);
            }
            catch (Exception ex)
            {
                Assert.Empty(ex.Message);
            }
        }

    }

}