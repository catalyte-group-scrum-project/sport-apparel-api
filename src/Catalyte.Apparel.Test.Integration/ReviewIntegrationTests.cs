﻿using Catalyte.Apparel.DTOs.Reviews;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Catalyte.Apparel.Test.Integration
{
    public class ReviewIntegrationTests : IntegrationTests
    {
        [Fact]
        public async Task CreateReviewAsyncReturnsReview()
        {
            ReviewDTO review = new ReviewDTO();
            review.ProductId = 1;
            review.Email = "thebest@gmail.com";
            review.Title = "Best product yet";
            review.Rating = 5;
            review.Comment = "couldn't be better if it tried";
            var response = await _client.PostAsJsonAsync("reviews", review);
            ReviewDTO content = null;
            content = await response.Content.ReadAsAsync<ReviewDTO>();
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(review.ProductId, content.ProductId);
            Assert.Equal(review.Email, content.Email);
            Assert.Equal(review.Title, content.Title);
            Assert.Equal(review.Rating, content.Rating);
            Assert.Equal(review.Comment, content.Comment);
        }
    }
}
