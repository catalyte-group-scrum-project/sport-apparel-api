﻿using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Catalyte.Apparel.Test.Integration
{
    public class PromoCodesIntegrationTests : IntegrationTests
    {
        [Fact]
        public async Task GetPromoCodes_Returns200()
        {
            var response = await _client.GetAsync("/promoCodes");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}