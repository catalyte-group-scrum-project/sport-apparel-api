﻿using Catalyte.Apparel.DTOs.Products;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Catalyte.Apparel.Test.Integration
{
    public class WishListIntegrationTests : IntegrationTests
    {

        [Fact]
        public async Task GetWishListItemsByEmailAsync()
        {
            var response = await _client.GetAsync("WishList/thebestuser@verizon.net");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            List<WishListDTO> content = null;
            content = await response.Content.ReadAsAsync<List<WishListDTO>>();
            Assert.Equal("thebestuser@verizon.net", content[0].UserEmail);
        }
        [Fact]
        public async Task CreateWishListItemAsyncReturnsCreatedProduct()
        {
            WishListDTO wishList = new WishListDTO();
            wishList.ProductId = 1;
            wishList.UserEmail = "thebest@gmail.com";
            wishList.Name = "Slim Golf Short";
            wishList.Description = "Golf, Kids, Slim";
            wishList.ImageSrc = "https://image.shutterstock.com/z/stock-photo-golf-equipment-130536464.jpg";
            wishList.Price = 51.70m;
            wishList.Demographic = "Kids";
            wishList.Category = "Golf";
            wishList.Type = "Short";
            var response = await _client.PostAsJsonAsync("WishList", wishList);
            WishListDTO content = null;
            content = await response.Content.ReadAsAsync<WishListDTO>();
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(wishList.ProductId, content.ProductId);
            Assert.Equal(wishList.UserEmail, content.UserEmail);
            Assert.Equal(wishList.Name, content.Name);
            Assert.Equal(wishList.Description, content.Description);
            Assert.Equal(wishList.ImageSrc, content.ImageSrc);
            Assert.Equal(wishList.Price, content.Price);
            Assert.Equal(wishList.Demographic, content.Demographic);
            Assert.Equal(wishList.Category, content.Category);
            Assert.Equal(wishList.Type, content.Type);
        }
        [Fact]
        public async Task GetPaginatedWishListByEmailReturns20Items()
        {
            var response = await _client.GetAsync("WishList/filter/thebestuser@verizon.net");
            List<WishListDTO> content = null;
            content = await response.Content.ReadAsAsync<List<WishListDTO>>();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal( 20 , content.Count);
        }
    }
}
