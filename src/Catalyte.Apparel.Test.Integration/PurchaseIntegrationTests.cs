using Catalyte.Apparel.DTOs.Purchases;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Catalyte.Apparel.Test.Integration
{
    public class PurchaseIntegrationTests : IntegrationTests
    {

        [Fact]
        public async Task GetPurchasesByEmail_GivenExistingEmail_Returns200()
        {
            var response = await _client.GetAsync("/purchases/asmith@aol.com");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            List<PurchaseDTO> content = null;
            content = await response.Content.ReadAsAsync<List<PurchaseDTO>>();
            Assert.Equal("asmith@aol.com", content[0].BillingAddress.Email);
        }
        [Fact]
        public async Task GetPurchasesByEmail_NoEmailSpecified_Returns400()
        {
            var response = await _client.GetAsync("/purchases");
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
        [Fact]
        public async Task GetPurchasesByEmail_GivenNonExistingEmail_Returns404()
        {
            var response = await _client.GetAsync("/purchases/bobbert@gmail.com");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
        [Fact]
        public async Task GetPurchasesByEmail_GivenExistingEmailNoPurchases_Returns200()
        {
            var response = await _client.GetAsync("/purchases/bobbert@hotmail.com");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            List<PurchaseDTO> content = null;
            content = await response.Content.ReadAsAsync<List<PurchaseDTO>>();
            Assert.Empty(content);
        }
        [Fact]
        public async Task GetPurchasesByEmail_GivenEmailNotURL_Returns400()
        {
            var response = await _client.GetAsync("/purchases/bobbert%hotmail.com");
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

    }
}
