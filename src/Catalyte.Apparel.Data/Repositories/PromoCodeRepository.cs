﻿using Catalyte.Apparel.Data.Context;
using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Data.Repositories
{
    /// <summary>
    /// This class handles methods for making requests to the promo code repository.
    /// </summary>
    public class PromoCodeRepository : IPromoCodeRepository
    {
        private readonly IApparelCtx _ctx;

        public PromoCodeRepository(IApparelCtx ctx)
        {
            _ctx = ctx;
        }

        public async Task<PromoCode> CreatePromoCodeAsync(PromoCode promoCode)
        {
            _ctx.PromoCodes.Add(promoCode);
            await _ctx.SaveChangesAsync();

            return promoCode;
        }
    }
}
