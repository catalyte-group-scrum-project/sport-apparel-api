﻿using Catalyte.Apparel.Data.Context;
using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Catalyte.Apparel.Data.Repositories
{
    public class LineItemRepository : ILineItemRepository
    {
        private readonly IApparelCtx _ctx;
        public LineItemRepository(IApparelCtx ctx)
        {
            _ctx = ctx;
        }


        public async Task<IEnumerable<LineItem>> GetLineItemsAsync()
        {
            return await _ctx.LineItems.ToListAsync();
        }
    }
}
