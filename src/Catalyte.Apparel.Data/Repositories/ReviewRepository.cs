﻿using System;
using Catalyte.Apparel.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Data.Context;
using Catalyte.Apparel.Data.Filters;
using Microsoft.EntityFrameworkCore;
using Catalyte.Apparel.Data.Filters;

namespace Catalyte.Apparel.Data.Repositories
{
    /// <summary>
    /// This class handles methods for making requests to the review repository.
    /// </summary>
    public class ReviewRepository : IReviewRepository
    {
        private readonly IApparelCtx _ctx;

        public ReviewRepository(IApparelCtx ctx)
        {
            _ctx = ctx;
        }

        public async Task<Review> CreateReviewAsync(Review review)
        {
            _ctx.Reviews.Add(review);
            await _ctx.SaveChangesAsync();

            return review;
        }

        public async Task<IEnumerable<Review>> GetReviewsAsync()
        {
            return await _ctx.Reviews.ToListAsync();
        }

        public async Task<IEnumerable<Review>> GetReviewsByProductIdAsync(int productId)
        {
            return await _ctx.Reviews.AsQueryable().WhereFilteredReviewEquals(productId).ToListAsync();
        }

        public async Task<Review> GetReviewByReviewIdAsync(int id)
        {
            return await _ctx.Reviews.FindAsync(id);
        }

        public async Task<Review> DeleteReviewAsync(Review reviewToDelete)
        {
            _ctx.Reviews.Remove(reviewToDelete);
            await _ctx.SaveChangesAsync();

            return reviewToDelete;
        }

        public async Task<List<Review>> GetReviewsByEmailAsync(string email)
        {
            List<Review> reviews = new List<Review>();
            reviews = await _ctx.Reviews.AsQueryable().WhereReviewEmailEquals(email).ToListAsync();

            return reviews;
        }

    }
}
