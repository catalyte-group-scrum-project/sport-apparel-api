﻿using Catalyte.Apparel.Data.Context;
using Catalyte.Apparel.Data.Filters;
using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Data.Repositories
{
    /// <summary>
    /// This class handles methods for making requests to the WishList repository.
    /// </summary>
    public class WishListRepository : IWishListRepository
    {
        private readonly IApparelCtx _ctx;

        public WishListRepository(IApparelCtx ctx)
        {
            _ctx = ctx;
        }

        public async Task<WishList> CreateWishListAsync(WishList wishListItemToCreate)
        {
            _ctx.WishList.Add(wishListItemToCreate);
            await _ctx.SaveChangesAsync();

            return wishListItemToCreate;
        }
        public async Task<IEnumerable<WishList>> GetWishListAsync()
        {
            return await _ctx.WishList.ToListAsync();
        }

        public async Task<List<WishList>> GetWishListItemsByEmailAsync(string email)
        {
            return await _ctx.WishList.AsQueryable().WhereWishListEmailEquals(email).ToListAsync();
        }
        public async Task<WishList> GetWishListByProductIdAsync(int productId)
        {
            return await _ctx.WishList.FindAsync(productId);
        }
    }
}
