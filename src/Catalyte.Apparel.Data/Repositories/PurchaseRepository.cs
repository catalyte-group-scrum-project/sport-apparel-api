﻿using Catalyte.Apparel.Data.Context;
using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Data.Filters;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace Catalyte.Apparel.Data.Repositories
{
    /// <summary>
    /// This class handles methods for making requests to the purchase repository.
    /// </summary>
    public class PurchaseRepository : IPurchaseRepository
    {
        private readonly ILogger<PurchaseRepository> _logger;
        private readonly IApparelCtx _ctx;
       

        public PurchaseRepository(ILogger<PurchaseRepository> logger, IApparelCtx ctx)
        {
            _logger = logger;
            _ctx = ctx;
            
        }

        public async Task<List<Purchase>> GetPurchasesByEmailAsync(string email, bool[] userExists)
        {
            User user;
            user = await _ctx.Users.AsQueryable().WhereUserEmailEquals(email).SingleOrDefaultAsync();
            if (user == default)
            {
                userExists[0] = false;
                return new List<Purchase>();
            }
            else
            {
                userExists[0] = true;
            }
            return await _ctx.Purchases.AsQueryable().WherePurchasesEmailEquals(email).ToListAsync();
        
        }


        /// <summary>
        /// This class handles methods for making creating purchase requests.
        /// Will only create a purchase if all items in purchase are of active status.
        /// </summary>
        /// <param name="purchase"></param>
        /// <returns></returns>

        public async Task<Purchase> CreatePurchaseAsync(Purchase purchase)
        {         
            await _ctx.Purchases.AddAsync(purchase);
        
            List<Product> purchaseProduct = new();
            foreach(var product in purchase.LineItems)
            {
                await _ctx.Products.FindAsync(product.ProductId);
                purchaseProduct.Add(product.Product);
            }
 
            foreach(var item in purchaseProduct)
            {
                if(item.Active == false)
                {
                    return purchase;
                }
            }

            var CreditCard = new CreditCard
            {
                CardNumber = purchase.CardNumber,
                CVV = purchase.CVV,
                CardHolder = purchase.CardHolder,
                Expiration = purchase.Expiration
            };

            try
            {
                Catalyte.Apparel.Data.Model.CreditCard.CreditCardValidation.Validation(CreditCard);
            }
            catch (Exception)
            {
                return purchase;
            }


            await _ctx.SaveChangesAsync();

            return purchase;
        }
    }
}
