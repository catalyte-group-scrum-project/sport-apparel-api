﻿using Catalyte.Apparel.Data.Context;
using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Data.Filters;
using Catalyte.Apparel.DTOs.Products;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using System.Linq.Expressions;
using System.Reflection;

namespace Catalyte.Apparel.Data.Repositories
{
    /// <summary>
    /// This class handles methods for making requests to the product repository.
    /// </summary>
    public class ProductRepository : IProductRepository
    {
        private readonly IApparelCtx _ctx;

        public ProductRepository(IApparelCtx ctx)
        {
            _ctx = ctx;
        }

        public async Task<Product> GetProductByIdAsync(int productId)
        {
            return await _ctx.Products.FindAsync(productId); 
        }
        public async Task<List<Product>> GetFilteredProductsAsync(ProductFilterDTO filter)
        {
 
            return await _ctx.Products.AsQueryable().WhereFilteredProductEquals(filter).ToListAsync();
        }


        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            return await _ctx.Products.ToListAsync();
        }

        public async Task<Product> CreateProductAsync(Product newProduct)
        {
            _ctx.Products.Add(newProduct);
            await _ctx.SaveChangesAsync();

            return newProduct;
        }
    }

}
