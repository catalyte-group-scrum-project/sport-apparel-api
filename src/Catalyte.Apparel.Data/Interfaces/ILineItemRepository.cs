﻿using Catalyte.Apparel.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Data.Interfaces
{
    public interface ILineItemRepository
    {
        Task<IEnumerable<LineItem>> GetLineItemsAsync();
    }
}
