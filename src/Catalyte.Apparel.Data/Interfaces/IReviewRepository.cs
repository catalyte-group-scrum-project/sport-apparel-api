﻿using Catalyte.Apparel.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Data.Interfaces
{
    /// <summary>
    /// This interface provides an abstraction layer for review repository methods.
    /// </summary>
    public interface IReviewRepository
    {
        Task<Review> CreateReviewAsync(Review review);
       
        Task<IEnumerable<Review>> GetReviewsAsync();

        Task<Review> GetReviewByReviewIdAsync(int id);

        Task<IEnumerable<Review>> GetReviewsByProductIdAsync(int productId);

        Task<Review> DeleteReviewAsync(Review reviewToDelete);

        Task<List<Review>> GetReviewsByEmailAsync(string email);
    }
}
