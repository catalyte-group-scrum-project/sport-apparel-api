﻿using Catalyte.Apparel.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Data.Interfaces
{
    /// <summary>
    /// This interface provides an abstraction layer for WishList repository methods.
    /// </summary>
    public interface IWishListRepository
    {
        Task<WishList> CreateWishListAsync(WishList wishListItemToCreate);
        Task<IEnumerable<WishList>> GetWishListAsync();
        Task<List<WishList>> GetWishListItemsByEmailAsync(string email);
        Task<WishList> GetWishListByProductIdAsync(int productId);
    }
}
