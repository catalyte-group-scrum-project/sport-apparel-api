﻿using Catalyte.Apparel.Data.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Catalyte.Apparel.Data.SeedData
{
    /// <summary>
    /// This class provides tools for generating random purchases.
    /// </summary>
    public class PurchasesFactory
    {
        Random _rand = new();

        private readonly List<string> _emails = new()
        {
            "jjones@gmail.com",
            "asmith@aol.com",
            "thebestuser@verizon.net",
            "bobbert@hotmail.com"
        };

        private readonly List<string> _firstNames = new()
        {
            "John",
            "Alice",
            "Jason",
            "Bob"
        };

        private readonly List<string> _lastNames = new()
        {
            "Jones",
            "Smith",
            "Patel",
            "Bert"
        };

        private List<string> _streets = new()
        {
            "111 main",
            "123 elm",
            "404 oak",
            "201 first"
        };

        private readonly List<string> _cities = new()
        {
            "Baltimore",
            "Denver",
            "Portland",
            "Annapolis"
        };
        private readonly List<string> _zipCodes = new()
        {
            "21228",
            "01234",
            "76543",
            "21041"
        };
        private readonly List<string> _states = new()
        {
            "MD",
            "CO",
            "OR",
            "MD"
        };

        private readonly List<string> _cardNumber = new()
        {
            "1234567812345678",
            "4321876543218765",
            "3456567834565678",
            "9870437598704375"
        };

        private readonly List<string> _cvv = new()
        {
            "404",
            "202",
            "611",
            "923"
        };

        private readonly List<string> _expiration = new()
        {
            "01/24",
            "04/23",
            "08/22",
            "12/24"
        };

        private readonly List<string> _phone = new()
        {
            "410-222-5252",
            "703-505-7768",
            "240-647-3030",
            "443-717-3456"
        };

        /// <summary>
        /// Generates a number of products based on input.
        /// </summary>
        /// <param name="numberOfProducts">The number of products to generate.</param>
        /// <returns>A list of random products.</returns>
        public List<Purchase> GeneratePurchases(int numberOfPurchases)
        {

            var purchaseList = new List<Purchase>();

            for (var i = 0; i < numberOfPurchases; i++)
            {
                purchaseList.Add(CreateNextPurchase(i + 1));
            }

            return purchaseList;
        }

        /// <summary>
        /// Uses seed data to build a purchase.
        /// </summary>
        /// <param name="id">ID to assign to the purchase.</param>
        /// <returns>An incrementally generated purchase.</returns>
        private Purchase CreateNextPurchase(int id)
        {
            int index = (id - 1)%3;
            return new Purchase
            {
                Id = id,
                BillingStreet  = _streets[index],
                BillingCity = _cities[index],
                BillingZip = _zipCodes[index],
                BillingPhone = _phone[index],   
                DeliveryFirstName = _firstNames[index],
                DeliveryLastName = _lastNames[index],
                DeliveryStreet = _streets[index],
                DeliveryCity = _cities[index],
                DeliveryState = _states[index],
                DeliveryZip = Convert.ToInt32(_zipCodes[index]),
                BillingEmail = _emails[index],               
                DateCreated = DateTime.UtcNow,
                DateModified = DateTime.UtcNow,
                CardHolder = _firstNames[index] + " " + _lastNames[index],
                CVV = _cvv[index],
                CardNumber = _cardNumber[index],
                Expiration = _expiration[index]
            };
        }

    }
}
