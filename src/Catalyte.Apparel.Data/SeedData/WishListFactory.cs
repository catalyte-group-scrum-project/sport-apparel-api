﻿using Catalyte.Apparel.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Data.SeedData
{
    /// <summary>
    /// This class provides tools for generating random reviews.
    /// </summary>
    public class WishListFactory
    {
        Random _rand = new();

        private readonly List<string> _demographics = new()
        {
            "Men",
            "Women",
            "Kids"
        };
        private readonly List<string> _categories = new()
        {
            "Golf",
            "Soccer",
            "Basketball",
            "Hockey",
            "Football",
            "Running",
            "Baseball",
            "Skateboarding",
            "Boxing",
            "Weightlifting"
        };

        private readonly List<string> _adjectives = new()
        {
            "Lightweight",
            "Slim",
            "Shock Absorbing",
            "Exotic",
            "Elastic",
            "Fashionable",
            "Trendy",
            "Next Gen",
            "Colorful",
            "Comfortable",
            "Water Resistant",
            "Wicking",
            "Heavy Duty"
        };

        private readonly List<string> _types = new()
        {
            "Pant",
            "Short",
            "Shoe",
            "Glove",
            "Jacket",
            "Tank Top",
            "Sock",
            "Sunglasses",
            "Hat",
            "Helmet",
            "Belt",
            "Visor",
            "Shin Guard",
            "Elbow Pad",
            "Headband",
            "Wristband",
            "Hoodie",
            "Flip Flop",
            "Pool Noodle"
        };

        /// <summary>
        /// Selects a random title from the '_titles' list
        /// </summary>
        /// <returns>A random string from the _titles list</returns>
        private string GetDemographic()
        {
            return _demographics[_rand.Next(0, _demographics.Count)];
        }
        /// <summary>
        /// Generates a random int from the given min and max valies
        /// </summary>
        /// <param name="min">Lowst random number generated</param>
        /// <param name="max">Highest random number generated</param>
        /// <returns></returns>
        public int GetRandomInt(int min, int max)
        {
            return _rand.Next(min, max);
        }
        /// <summary>
        /// Generates a number of random wishlist products based on input.
        /// </summary>
        /// <param name="numberOfProducts">The number of random wishlist products to generate.</param>
        /// <returns>A list of random wishlist products.</returns>
        public List<WishList> GenerateRandomWishList(int numberOfProducts)
        {

            var WishList = new List<WishList>();

            for (var i = 0; i < numberOfProducts; i++)
            {
                WishList.Add(CreateRandomWishList(i + 1));
            }

            return WishList;
        }

        /// <summary>
        /// Uses random generators to build a wishlist product.
        /// </summary>
        /// <param name="id">ID to assign to the wishlist product.</param>
        /// <returns>A randomly generated wishlist product.</returns>
        private WishList CreateRandomWishList(int id)
        {
            string cata = _categories[_rand.Next(0, _categories.Count)];
            string demogr = GetDemographic();
            string adj = _adjectives[_rand.Next(0, _adjectives.Count)];
            string typ = _types[_rand.Next(0, _types.Count)];
            decimal price = (decimal)GetRandomInt(1, 10000) / 100;
            string img = "https://www.signfix.com.au/wp-content/uploads/2017/09/placeholder-600x400.png";


            return new WishList
            {
                Id = id,
                ProductId = GetRandomInt(1, 100),
                UserEmail = "thebestuser@verizon.net",
                Name = $"{adj} {cata} {typ} ",
                Description = $"{cata}, {demogr}, {adj} ",
                Demographic = demogr,
                Category = cata,
                Type = typ,
                DateCreated = DateTime.UtcNow,
                DateModified = DateTime.UtcNow,
                ImageSrc = img,
                Price = price
            };
        }
    }
}
