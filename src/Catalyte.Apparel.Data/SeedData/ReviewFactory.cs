﻿using Catalyte.Apparel.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Data.SeedData
{
    /// <summary>
    /// This class provides tools for generating random reviews.
    /// </summary>
    public class ReviewFactory
    {
        Random _rand = new();

        private readonly List<string> _titles = new()
        {
            "Exquisite!",
            "Just what I was looking for!",
            "Exactly as pictured!",
            "Prompt shipping!",
            "So luxurious!",
            "Would order these again!",
            "Well made product, perfect fit!",
            "Best sports apparel in the Outer Rim!",
            "Sturdy as beskar!",
            "More powerful than you could ever imagine.",
            "Gooooooood"
        };

        private readonly List<string> _comments = new()
        {
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "Vivamus non cursus ante, a suscipit enim.",
            "Integer ut dolor feugiat enim vehicula vehicula ut id mi.",
            "My sports apparel plans are now complete...",
            "You'll never find a more thriving hive of sports and apparel.",
            "Once you start down the sports apparel path, forever will it dominate your destiny."
        };
       
        private readonly List<string> _emails = new()
        {
            "jjones@gmail.com",
            "asmith@aol.com",
            "thebestuser@verizon.net",
            "bobbert@hotmail.com",
            "cphelps@catalyte.io",
            "zplanalp@catalyte.io",
            "ahyde@catalyte.io"
        };

        private readonly List<int> _ratings = new()
        {
            1,
            2,
            3,
            4,
            5
        };

        /// <summary>
        /// Selects a random title from the '_titles' list
        /// </summary>
        /// <returns>A random string from the _titles list</returns>
        private string GetRandomTitle()
        {
            return _titles[_rand.Next(0, _titles.Count)];
        }

        /// <summary>
        /// Selects a random comment from the '_comments' list
        /// </summary>
        /// <returns>A random string from the _comments list</returns>
        private string GetRandomComment()
        {
            return _comments[_rand.Next(0, _comments.Count)];
        }

        /// <summary>
        /// Selects a random email from the '_emails' list
        /// </summary>
        /// <returns>A random string from the _emails list</returns>
        private string GetRandomEmail()
        {
            return _emails[_rand.Next(0, _emails.Count)];
        }

        ///<sumary>
        /// Selects a random rating from the '_ratings' list
        /// </sumary>
        /// <returns>A random int from the _ratinds list</returns>
        private int GetRandomRating()
        {
            return _ratings[_rand.Next(0, _ratings.Count)];
        }

        /// <summary>
        /// Generates a random DateTime obj between range set by params
        /// </summary>
        /// <returns>random DateTime obj</returns>
        DateTime GetDateTime(int minYear, int maxYear)
        {
            int randomMonth = _rand.Next(2, 3);
            int randomYear = _rand.Next(minYear, maxYear);
            int randomDay = _rand.Next(1, 28);
            int randomHour = _rand.Next(0, 24);
            int randomMin = _rand.Next(0, 60);
            int randomSec = _rand.Next(0, 60);
            return new DateTime(randomYear, randomMonth, randomDay, randomHour, randomMin, randomSec);
        }

        /// <summary>
        /// Generates a number of random reivews based on input.
        /// </summary>
        /// <param name="numberOfReviews>The number of random products to generate.</param>
        /// <returns>A list of random reviews.</returns>
        public List<Review> GenerateReviews(int numberOfReviews)
        {

            var reviewList = new List<Review>();

            for (var i = 0; i < numberOfReviews; i++)
            {
                reviewList.Add(CreateRandomReview(i + 1));
            }

            return reviewList;
        }

        /// <summary>
        /// Uses random generators to build a review.
        /// </summary>
        /// <param name="id">ID to assign to the product.</param>
        /// <returns>A randomly generated review.</returns>
        private Review CreateRandomReview(int id)
        {

            return new Review
            {
                Id = id,
                ProductId = _rand.Next(1, 1000),
                Title = GetRandomTitle(),
                Comment = GetRandomComment(),
                Email = GetRandomEmail(),
                Rating = GetRandomRating(),
                DateCreated = GetDateTime(2015, 2019)
            };
        }

    }
}
