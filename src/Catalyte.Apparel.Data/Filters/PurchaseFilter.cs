﻿using Catalyte.Apparel.Data.Model;
using System.Linq;

namespace Catalyte.Apparel.Data.Filters
{
    /// <summary>
    /// Filter collection for user context queries.
    /// </summary>
    public static class PurchasesFilter
    {
        public static IQueryable<Purchase> WherePurchasesEmailEquals(this IQueryable<Purchase> purchases, string email)
        {
            return purchases.Where(p => p.BillingEmail == email).AsQueryable();
        }
    }
}
