﻿using Catalyte.Apparel.Data.Model;
using System.Linq;

namespace Catalyte.Apparel.Data.Filters
{
    /// <summary>
    /// Filter collection for user context queries.
    /// </summary>
    public static class LineItemFilter
    {
        public static IQueryable<LineItem> WherePurchaseIdEquals(this IQueryable<LineItem> lineItems, int purchaseId)
        {
            return lineItems.Where(l => l.PurchaseId == purchaseId).AsQueryable();
        }
    }
}
