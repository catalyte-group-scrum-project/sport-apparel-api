﻿using Catalyte.Apparel.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Globalization;
using Catalyte.Apparel.DTOs.Reviews;



/// <summary>
/// Filter collection for review context queries.
/// </summary>
namespace Catalyte.Apparel.Data.Filters
{

    /// <summary>
    /// Asynchronously retrieves filtered reviews from the database.
    /// </summary>
    /// <returns>Filtered reviews from the database.</returns>
    public static class ReviewFilter 
    {
        public static IQueryable<Review> WhereFilteredReviewEquals(this IQueryable<Review> reviews, int productId)
        {
            //Create the predicate, once filled with all filters, predicate will be passed into Where() only once
            var predicate = PredicateBuilder.True<Review>();
            var innerPredicate = PredicateBuilder.False<Review>();

            foreach (Review review in reviews)
            {
                innerPredicate = innerPredicate.Or(p => p.ProductId == productId);
            }
            predicate = predicate.And(innerPredicate);
            reviews = reviews.Where(predicate).AsQueryable();
            return reviews;
        }
    }
}
