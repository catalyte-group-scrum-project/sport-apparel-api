﻿using Catalyte.Apparel.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Data.Filters
{
    /// <summary>
    /// Filter collection for WishList context queries.
    /// </summary>
    public static class WishListFilters
    {
        public static IQueryable<WishList> WhereWishListEmailEquals(this IQueryable<WishList> WishLists, string email)
        {
            return WishLists.Where(u => u.UserEmail == email).AsQueryable();
        }
    }
}
