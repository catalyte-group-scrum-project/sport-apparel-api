﻿using Catalyte.Apparel.Data.Model;
using System.Linq;

namespace Catalyte.Apparel.Data.Filters
{
    /// <summary>
    /// Filter collection for user context queries.
    /// </summary>
    public static class ReviewEmailFilter
    {
        public static IQueryable<Review> WhereReviewEmailEquals(this IQueryable<Review> reviews, string email)
        {
            return reviews.Where(r => r.Email == email).AsQueryable();
        }
    }
}
