﻿using Newtonsoft.Json;
using System;

namespace Catalyte.Apparel.Data.Model
{
    /// <summary>
    /// Describes the object for a promo code.
    /// </summary>
    public class PromoCode : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public bool? IsPercent { get; set; }
        public decimal Rate { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public override bool Equals(object obj)
        {
            return obj is PromoCode code &&
                   Title == code.Title &&
                   Description == code.Description &&
                   IsPercent == code.IsPercent &&
                   Rate == code.Rate;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Title, Description, IsPercent, Rate);
        }
    }
}
