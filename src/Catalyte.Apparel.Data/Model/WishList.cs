﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Data.Model
{
    public class WishList : BaseEntity

    {
        [Column("ProductID")]
        public int ProductId { get; set; }

        [Column("UserEmail")]
        public string UserEmail { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ImageSrc { get; set; }

        public decimal Price { get; set; }

        public string Demographic { get; set; }

        public string Category { get; set; }

        public string Type { get; set; }
    }
}
