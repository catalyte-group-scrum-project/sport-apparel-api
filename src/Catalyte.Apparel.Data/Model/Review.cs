﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Data.Model
{
    /// <summary>
    /// Generates review for product
    /// </summary>
    public class Review : BaseEntity
    {

        [Column("ProductId")]
        public int ProductId { get; set; }

        public string Title { get; set; }

        public string Comment { get; set; }

        public int Rating { get; set; }

        public string Email { get; set; }


        public override bool Equals(object obj)
        {
            return obj is Review review &&
                   Id == review.Id &&
                   DateCreated == review.DateCreated &&
                   DateModified == review.DateModified &&
                   ProductId == review.ProductId &&
                   Title == review.Title &&
                   Comment == review.Comment &&
                   Rating == review.Rating &&
                   Email == review.Email;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, DateCreated, DateModified, ProductId, Title, Comment, Rating, Email);
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}