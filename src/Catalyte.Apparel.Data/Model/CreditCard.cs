﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;

namespace Catalyte.Apparel.Data.Model
{
    /// <summary>
    /// Describes the credit card object for transactions.
    /// </summary>
    public class CreditCard
    {
        public string CardNumber { get; set; }

        public string CVV { get; set; }

        public string Expiration { get; set; }

        public string CardHolder { get; set; }

        private sealed class CreditCardEqualityComparer : IEqualityComparer<CreditCard>
        {
            public bool Equals(CreditCard x, CreditCard y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return x.CardNumber == y.CardNumber && x.CVV == y.CVV && x.Expiration == y.Expiration && x.CardHolder == y.CardHolder;
            }

            public int GetHashCode(CreditCard obj)
            {
                return HashCode.Combine(obj.CardNumber, obj.CVV, obj.Expiration, obj.CardHolder);
            }
        }

        public static IEqualityComparer<CreditCard> CreditCardComparer { get; set; } = new CreditCardEqualityComparer();


        /// <summary>
        /// Function to validate CreditCards 
        /// <summary>
        public class CreditCardValidation
        {

            public static void Validation(CreditCard obj)
            {
                var exceptions = new List<String>();
                var CardNumberLength = obj.CardNumber.Trim().Replace(" ", "").Length;

                if (string.IsNullOrEmpty(obj.CardNumber) || string.IsNullOrEmpty(obj.CVV) || string.IsNullOrEmpty(obj.Expiration) || string.IsNullOrEmpty(obj.CardHolder))
                {
                    exceptions.Add("Credit card inputs cannot be empty");
                }

                if (CardNumberLength < 15 | CardNumberLength > 16)
                {
                    exceptions.Add("CardNumber must be 15 or 16 digits");
                }

                if (!Regex.IsMatch(obj.CardNumber, @"^[3][4][0-9]{2}\s?[0-9]{6}\s?[0-9]{5}$|^[3][7][0-9]{2}\s?[0-9]{6}\s?[0-9]{5}$|^[4][0-9]{3}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$|^[5][1-5][0-9]{2}\s?[0-9]{4}\s?[0-9]{4}\s?[0-9]{4}$"))
                {
                    exceptions.Add("2022 Sports Apparel, Inc. only accepts Visa, Mastercard, or AmEx");
                }

                if (obj.CVV.Length < 3 | obj.CVV.Length > 4)
                {
                    exceptions.Add("CVV must be 3 or 4 digits");
                }

                if (!string.IsNullOrEmpty(obj.Expiration))
                {
                    var currentDate = DateTime.Parse(DateTime.Now.ToString("MM/yy"));
                    var experationDate = DateTime.ParseExact(obj.Expiration, "MM/yy", null);

                    if (currentDate.Year > experationDate.Year || currentDate.Year == experationDate.Year && currentDate.Month > experationDate.Month)
                    {
                        exceptions.Add("Card must not be expired");
                    }

                }

                if (exceptions.Count > 0)
                {
                    throw new BadRequestException(string.Join(". ", exceptions));
                }

            }
        }

    }
}
